<?php

namespace App\Http\Middleware;

use Closure;

class RedirectIfNotSuperAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(auth()->user()->super_admin != 1){
            return redirect()->to('admin');
        }
        return $next($request);
    }
}
