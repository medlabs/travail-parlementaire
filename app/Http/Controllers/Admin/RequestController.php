<?php

namespace App\Http\Controllers\Admin;

use App\Mail\NotifApplicant;
use App\Models\Applicant;
use App\Models\Category;
use App\Models\Comment;
use App\Models\District;
use App\Models\Ministry;
use App\Models\Status;
use App\Models\UserType;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;

class RequestController extends Controller
{
    public function requests(Request $request)
    {
        $reqs = $request->all();
        $status = Status::Active()->get();
        $districts = District::Active()->get();
        $users = User::all();
        $applicants = Applicant::whereHas('requests')->get();

        return view('admin.request.index', compact('districts', 'status', 'users', 'reqs', 'applicants'));
    }

    public function show_request($request_id)
    {
        $request = \App\Models\Request::where('id', $request_id)->firstOrFail();

        $districts = District::Active()->get();
        $categories = Category::Active()->get();
        $ministries = Ministry::Active()->get();

        $permission = $permissions = json_decode(auth()->user()->role->permissions);
        if (
            (!auth()->user()->role->super_admin) &&
            (!isset($permission->all_requests->is_read)) &&
            (($request->user_id == null) && !isset($permission->new_request->is_read)) &&
            (($request->closed == 1) && !isset($permission->closed_request->is_read)) &&
            (($request->user_id != auth()->user()->id) && !isset($permission->selected_request->is_read))
        ) {
            abort(403);
        }

        $update = false;
        if (
            (auth()->user()->role->super_admin) ||
            (isset($permission->all_requests->is_edit)) ||
            (($request->user_id == null) && isset($permission->new_request->is_edit)) ||
            (($request->closed == 1) && isset($permission->closed_request->is_edit)) ||
            (($request->user_id != auth()->user()->id) && isset($permission->selected_request->is_edit))
        ) {
            $update = true;
        }

        $status = Status::Active()->get();
        $user_type = UserType::all();
        $users = User::where('district_id', $request->district_id)
            ->get();

        return view(
            'admin.request.show_request',
            compact('request', 'status', 'users', 'user_type', 'permission', 'update', 'districts', 'categories','ministries')
        );
    }

    public function update_request(Request $request)
    {
        $request_user = \App\Models\Request::where('id', $request->request_id)->firstOrFail();

        $permission = $permissions = json_decode(auth()->user()->role->permissions);

        if (
            (!auth()->user()->role->super_admin) &&
            (!isset($permission->all_requests->is_edit)) &&
            (($request_user->user_id == null) && !isset($permission->new_request->is_edit)) &&
            (($request_user->closed == 1) && !isset($permission->closed_request->is_edit)) &&
            (($request_user->user_id != auth()->user()->id) && !isset($permission->selected_request->is_edit))
        ) {
            abort(403);
        }

        $old_status = Status::where('id', $request_user->status_id)->first();
        $new_status = Status::where('id', $request->status)->first();

        $old_admin = User::where('id', $request_user->user_id)->first();
        $new_admin = User::where('id', $request->user)->first();

        $request_user->update(
            [
                'status_id' => $request->status,
                'user_id' => $request->user,
                'role_id' => $request->user_type_id,
                'closed' => $request->closed == 1 ? 1 : 0,
                'request_information' => $request->request_information ? 1 : 0
            ]
        );

        if ($new_status) {
            if ($new_status->parent_status == null || $request->request_information) {
                Mail::to($request_user->applicant->email)->send(new NotifApplicant());
            }
            if ($old_status != $new_status) {
                Comment::create(
                    [
                        'request_id' => $request->request_id,
                        'user_id' => auth()->user()->id,
                        'applicant_id' => null,
                        'body' => auth()->user()->name . ' a changé le statut à ' . $new_status->name,
                        'hidden' => ($new_status->parent_status != null) ? 1 : 0
                    ]
                );
            }
        }

        if ($new_admin != $old_admin) {
            if ($new_admin != null) {
                Comment::create(
                    [
                        'request_id' => $request->request_id,
                        'user_id' => auth()->user()->id,
                        'applicant_id' => null,
                        'body' => auth()->user()->name . ' a assigné le ticket à ' . $new_admin->name,
                        'hidden' => 1
                    ]
                );
            }
        }

        session()->flash('message', 'Request has been updated!');
        return redirect()->back();
    }

}
