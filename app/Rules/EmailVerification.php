<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\Lang;
use Ixudra\Curl\Facades\Curl;

class EmailVerification implements Rule
{

    protected $isset;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->isset = true;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if (!empty($value)) {
            $response = Curl::to(config('app.api_check_mail_uri'))
                ->withData(
                    array(
                        'api_key' => 'a0475d18957d42d708d4fd9fbeea2384c963a8a6328f5765e9b93f2f3b345b44',
                        'email' => $value
                    )
                )
                ->asJson(true)
                ->get();
            return isset($response['result']) && $response['result'] == 'deliverable';
        } else {
            $this->isset = false;
            return false;
        }
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        if ($this->isset == false) {
            return Lang::get('validation.required' , [Lang::get('attributes.email')]);
        } else {
            return Lang::get('validation.email' , [Lang::get('attributes.email')]);
        }
    }
}
