<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class GovRequestComment extends Model
{

    protected $table = 'gov_request_comments';
    protected $fillable = ['id', 'gov_request_id', 'user_id', 'body', 'file', 'file_name'];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

}
