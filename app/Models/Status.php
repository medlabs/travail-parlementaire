<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{

    protected $fillable = ['name', 'parent_status', 'color', 'active'];

    public function parent()
    {
        return $this->hasMany(Status::class, 'parent_id', 'id');
    }

    public function scopeActive($query)
    {
        return $query->where('active', 1);
    }

}
