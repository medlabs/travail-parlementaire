<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;

class TrakApplicantCode implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $title;
    public $user_name;
    public $email;
    public $cin;
    public $key;
    public $created_at;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($title, $user_name, $email, $cin, $key, $created_at)
    {
        $this->title = $title;
        $this->user_name = $user_name;
        $this->email = $email;
        $this->cin = $cin;
        $this->key = $key;
        $this->created_at = $created_at;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Mail::to($this->email)->send(
            new \App\Mail\TrakApplicantCode(
                $this->title,
                $this->user_name,
                $this->cin,
                $this->key,
                $this->created_at
            )
        );
    }
}
