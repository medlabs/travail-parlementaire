<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Laravel') }}</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="{{ asset('bower_components/bootstrap/dist/css/bootstrap.min.css') }}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset('bower_components/font-awesome/css/font-awesome.min.css') }}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="{{ asset('bower_components/Ionicons/css/ionicons.min.css') }}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('_admin/css/AdminLTE.min.css') }}">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="{{ asset('_admin/css/skins/_all-skins.min.css') }}">

@yield('after_style')
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Google Font -->
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper" id="app">

    <header class="main-header">
        <!-- Logo -->
        <a href="{{ url('/admin') }}" class="logo">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <span class="logo-mini"><b>A</b>LT</span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg">{{ config('app.name') }}</span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </a>

            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <!-- User Account: style can be found in dropdown.less -->
                    <li class="dropdown user user-menu">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                           data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            {{ Auth::user()->name }} <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- Menu Footer-->
                            <li class="user-footer">
                                <div class="pull-right">
                                    <a  class="btn btn-default btn-flat" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                        document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        </ul>
                    </li>

                </ul>
            </div>
        </nav>
    </header>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
            <!-- sidebar menu: : style can be found in sidebar.less -->
            <ul class="sidebar-menu" data-widget="tree">
                <li>
                    <a href="{{ url('/admin') }}">Dashbord</a>
                </li>
                <li class="header">MAIN NAVIGATION</li>
                <li class="nav-item">
                    <a href="{{ route('requests') }}"
                       class="nav-link {{ (\Request::route()->getName()== 'requests')?'active':'' }} ">
                        Requests
                    </a>
                </li>
                @can('applicant')
                    <li class="nav-item">
                        <a href="{{ route('applicant') }}"
                           class="nav-link {{ (\Request::route()->getName()== 'applicant')?'active':'' }}">
                            Applicant
                        </a>
                    </li>
                @endcan
                @can('gov_request')
                    <li class="nav-item">
                        <a href="{{ route('gov_request') }}"
                           class="nav-link {{ (\Request::route()->getName()== 'gov_request')?'active':'' }}">
                            Gov Request
                        </a>
                    </li>
                @endcan
                @can('user_managemen_roles')
                    <li class="header">USERS MANAGEMENT</li>
                    @can('roles')
                        <li>
                            <a href="{{ route('user_types') }}"
                               class="nav-link {{ in_array(\Request::route()->getName(), ['user_types' ,'create_user_type','show_user_type'])?'active':'' }}">
                                Privileges Roles
                            </a>
                        </li>
                    @endcan
                    @can('user_management')
                        <li>

                            <a href="{{ route('admin_users') }}"
                               class="nav-link has-sub-menu {{ in_array(\Request::route()->getName(), ['admin_users' ,'create_admin_user'])?'active':'' }}">
                                Users
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('profiles') }}"
                               class="nav-link {{ in_array(\Request::route()->getName(), ['profiles','create_profile'])?'active':'' }}">

                                Profiles
                            </a>
                        </li>
                    @endcan
                @endcan
                @can('settings')
                    <li class="header">SETTINGS</li>
                    <li>
                        <a href="{{ route('categories') }}"
                           class="nav-link {{ in_array(\Request::route()->getName(), ['categories','create_category'])?'active':'' }}">
                            Categories
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('ministry') }}"
                           class="nav-link {{ in_array(\Request::route()->getName(), ['ministry','create_ministry'])?'active':'' }}">
                            Ministry
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('districts') }}"
                           class="nav-link {{ in_array(\Request::route()->getName(), ['districts','create_district'])?'active':'' }}">
                            Districts
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('status') }}"
                           class="nav-link {{ in_array(\Request::route()->getName(), ['status','create_status'])?'active':'' }}">
                            Status
                        </a>
                    </li>
                @endcan
                <li class="header">SUPER ADMIN</li>
                <li>
                    <a href="#">
                        Log Users Access
                    </a>
                </li>
                <li>
                    <a href="{{ url('logs') }}">
                        Logs
                    </a>
                </li>
            </ul>
        </section>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                @yield('section_title')
            </h1>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box box-primary">
                        <!-- /.box-header -->
                        <div class="box-body">
                            @yield('content')
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <!-- /.content-wrapper -->

    <footer class="main-footer">
        All rights reserved.
    </footer>

    <!-- Add the sidebar's background. This div must be placed
         immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="{{ asset('bower_components/jquery/dist/jquery.min.js') }}"></script>
<!-- Bootstrap 3.3.7 -->
<script src="{{ asset('bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<!-- FastClick -->
<script src="{{ asset('bower_components/fastclick/lib/fastclick.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('_admin/js/adminlte.min.js') }}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{ asset('_admin/js/demo.js') }}"></script>
<!-- Scripts -->
@if(\Request::route()->getName() == 'show_admin_request')
    <script src="{{ asset('js/app.js') }}" defer></script>
@else
    <script src="{{ asset('js/app.js') }}"></script>
@endif
@yield('after_script')
</body>
</html>
