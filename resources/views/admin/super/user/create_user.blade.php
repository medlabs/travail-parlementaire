@extends('admin.layouts.app')

@section('section_title')
    Add New Admin
@endsection

@section('content')

    @include('errors.errors')
    <form method="POST" action="{{ route('store_new_user') }}">
        @csrf
        <div class="form-group row">
            <label for="first_name" class="col-md-5 col-form-label text-md-right">
                {{ __('District') }} *
            </label>
            <div class="col-md-6">
                <select class="form-control{{ $errors->has('district') ? ' is-invalid' : '' }}"
                        name="district"
                        required
                >
                    <option>- Sélection votre Circonscription -</option>
                    @foreach($districts as $district)
                        <option value="{{ $district->id }}">{{ $district->name_en }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="form-group row">
            <label for="name" class="col-md-5 col-form-label text-md-right">
                {{ __('User Name') }} *
            </label>
            <div class="col-md-6">
                <input id="name"
                       type="text"
                       class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}"
                       name="name"
                       value="{{ old('name') }}" required>
            </div>
        </div>
        <div class="form-group row">
            <label for="email" class="col-md-5 col-form-label text-md-right">{{ __('E-Mail Address *') }}</label>
            <div class="col-md-6">
                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>
            </div>
        </div>
        <div class="form-group row">
            <label for="password" class="col-md-5 col-form-label text-md-right">{{ __('Password *') }}</label>
            <div class="col-md-6">
                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
            </div>
        </div>
        <hr style="opacity: .2">
        <div class="form-group row">
            <label for="first_name" class="col-md-5 col-form-label text-md-right">
                {{ __('Privilege') }} *
            </label>
            <div class="col-md-6">
                <select class="form-control{{ $errors->has('role_id') ? ' is-invalid' : '' }}"
                        name="role_id"
                        required
                >
                    <option>- Select User Type -</option>
                    @foreach($user_types as $user_type)
                        <option value="{{ $user_type->id }}">{{ $user_type->name }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <hr style="opacity: .2">
        <div class="form-group row">
            <div class="col-md-6 offset-5">
                <button type="submit" class="btn btn-primary">
                    {{ __('Add New Admin') }}
                </button>
            </div>
        </div>
    </form>

@endsection
