@extends('admin.layouts.app')

@section('after_style')
    <link href="https://cdn.datatables.net/v/dt/dt-1.10.18/sc-1.5.0/datatables.min.css" rel="stylesheet" type="text/css" />
@endsection

@section('section_title')
    Status
@endsection

@section('content')

    <div class="card shadow-sm">
        <div class="card-header">
            <h1 class="card-title"></h1>
        </div>
        <div class="card-body">
            @include('errors.errors')
            @if($flash = session('message'))
                <div class="alert alert-success">
                    <i class="fa fa-bell" aria-hidden="true"></i>
                    {{ $flash }}
                </div>
            @endif
            <table id="table" class="table table-striped table-bordered">
                <thead class="thead-dark">
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>color</th>
                    <th>Parent</th>
                    <th>Active</th>
                    <th style="width: 150px;">Created at</th>
                    <th>Action</th>
                </tr>
                </thead>
            </table>
            <div class="text-right" style="margin-top: 20px;">
                <a href="{{ route('create_status') }}" class="btn btn-lg btn-primary">Add New status</a>
            </div>
        </div>
    </div>

@endsection

@section('after_script')

    <script src="https://cdn.datatables.net/v/dt/dt-1.10.18/sc-1.5.0/datatables.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#table').DataTable({
                order: [0,'desc'],
                processing: true,
                serverSide: true,
                ajax: "{{ url('/api/admin/status_list') }}",
                columns: [
                    { "data": "id" },
                    { "data": "name" },
                    {
                        "data": "color",
                        "render": function(data){
                            return '<i class="fa fa-circle" style="font-size:20px; color:#'+data+';"></i>';
                        },
                        "searchable": false,
                        "orderable":false
                    },
                    {
                        "data": "parent",
                        "searchable": false,
                        "orderable":false
                    },
                    {
                        "data": "active",
                        "render": function(data){
                            if(data == 0){
                                return '<i class="fa fa-times-circle" title="Verified" style="font-size:20px; color:#e74c3c;"></i>'
                            }else{
                                return '<i class="fa fa-check-circle" title="Verified" style="font-size:20px; color:#badc58;"></i>';
                            }
                            return data;
                        },
                        "searchable": false,
                        "orderable":false
                    },
                    {
                        "data": "created_at",
                        "searchable": false,
                        "orderable":false,
                    },
                    {
                        "data": "action",
                        "searchable": false,
                        "orderable":false,
                    }
                ]
            });

        });

    </script>

@endsection
