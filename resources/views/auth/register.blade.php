@extends('layouts.app')
{{--
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-7">
            <div class="card shadow-sm ">
                <div class="card-header">
                    <h1 class="card-title">Créez un compte</h1>
                </div>
                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}" aria-label="{{ __('Register') }}">
                        @csrf
                        <div class="card shadow-sm">
                            <div class="card-header"><h2 class="card-title">{{ __('Mes Information') }}</h2></div>
                            <div class="card-body">
                                <div class="form-group row">
                                    <label for="first_name" class="col-md-5 col-form-label text-md-right">
                                        {{ __('Circonscription électorale') }} *
                                    </label>
                                    <div class="col-md-6">
                                        <select class="form-control{{ $errors->has('first_name') ? ' is-invalid' : '' }}"
                                                name="circonscription"
                                                required
                                        >
                                            <option>- Sélection votre Circonscription -</option>
                                            <option>France 1</option>
                                            <option>France 2</option>
                                            <option>Tunis 1</option>
                                            <option>Tunis 2</option>
                                            <option>Nabel</option>
                                        </select>
                                        @if ($errors->has('first_name'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('first_name') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <hr>
                                <div class="form-group row">
                                    <label for="name" class="col-md-5 col-form-label text-md-right">
                                        {{ __('Nom et Prenom') }} *
                                    </label>
                                    <div class="col-md-6">
                                        <input id="name"
                                               type="text"
                                               class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}"
                                               name="name"
                                               value="{{ old('name') }}" required autofocus>
                                        @if ($errors->has('name'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="gender" class="col-md-5 col-form-label text-md-right">{{ __('Genre') }} *</label>
                                    <div class="col-md-6">
                                        <select class="form-control{{ $errors->has('first_name') ? ' is-invalid' : '' }}"
                                                name="gender"
                                                required
                                        >
                                            <option>-- Sélection votre sexe --</option>
                                            <option value="Male">Homme</option>
                                            <option value="Female">Femme</option>
                                        </select>
                                        @if ($errors->has('first_name'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('first_name') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="cin" class="col-md-5 col-form-label text-md-right">
                                        {{ __('CIN') }} *
                                    </label>
                                    <div class="col-md-6">
                                        <input id="cin"
                                               type="text"
                                               class="form-control{{ $errors->has('cin') ? ' is-invalid' : '' }}"
                                               name="cin"
                                               value="{{ old('cin') }}" required autofocus>
                                        @if ($errors->has('cin'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('cin') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="phone_number" class="col-md-5 col-form-label text-md-right">
                                        {{ __('Numéro de téléphone') }} *
                                    </label>
                                    <div class="col-md-6">
                                        <input id="phone_number"
                                               type="text"
                                               class="form-control{{ $errors->has('cin') ? ' is-invalid' : '' }}"
                                               name="phone_number"
                                               value="{{ old('phone_number') }}" required autofocus>
                                        @if ($errors->has('phone_number'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('phone_number') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card shadow-sm">
                            <div class="card-header"><h2 class="card-title">{{ __('Information de conections') }}</h2></div>
                            <div class="card-body">
                                <div class="form-group row">
                                    <label for="email" class="col-md-5 col-form-label text-md-right">{{ __('Adresse email *') }}</label>
                                    <div class="col-md-6">
                                        <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>
                                        @if ($errors->has('email'))
                                            <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="password" class="col-md-5 col-form-label text-md-right">{{ __('Mot de passe *') }}</label>

                                    <div class="col-md-6">
                                        <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
                                        @if ($errors->has('password'))
                                            <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="password-confirm" class="col-md-5 col-form-label text-md-right">{{ __('Confirmer le mot de passe *') }}</label>

                                    <div class="col-md-6">
                                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <p>
                            En appuyant sur Inscription, vous acceptez nos Conditions générales, notre Politique d’utilisation des données et notre Politique d’utilisation des cookies.
                        </p>
                        <div class="form-group">
                            <button type="submit" class="btn btn-lg btn-primary">
                                {{ __('Créer mon Compte Personnel') }}
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-5">
            <div class="card shadow-sm">
                <div class="card-body">
                    <h1 class="title">Pourquoi créer un compte ?</h1>
                    <h2 class="sub_title">Gagnez du temps</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda corporis deserunt dolore fugit inventore labore minus nostrum nulla officia quasi quis quo reiciendis suscipit, vel voluptatum. Dolores laborum nulla officiis?</p>
                    <h2 class="sub_title">Soyez les premiers informés</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Doloribus et eum fuga perferendis praesentium quibusdam sed sunt! Animi laboriosam neque quos sequi? Distinctio itaque magnam optio quae quidem totam voluptatem.</p>
                    <h2 class="sub_title">Visibilité</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus eveniet labore maxime numquam rem reprehenderit, similique velit. Asperiores cum doloremque enim in magnam minima nostrum odit quaerat, repellat ut vitae.</p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
--}}
