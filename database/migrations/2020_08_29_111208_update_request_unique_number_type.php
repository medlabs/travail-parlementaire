<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateRequestUniqueNumberType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('gov_requests', function (Blueprint $table) {
            $table->string('request_unique_number')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('gov_requests', function (Blueprint $table) {
            $table->unsignedBigInteger('request_unique_number')->change();
        });
    }
}
