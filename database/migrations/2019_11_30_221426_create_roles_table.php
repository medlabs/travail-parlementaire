<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRolesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('roles', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('super_admin')->default(0);
            $table->text('permissions')->nullable();
            $table->timestamps();
        });

        \Illuminate\Support\Facades\DB::table('roles')->insert([
            'id' => 1,
            'name' => 'Super Admin',
            'super_admin' => 1
        ]);

        \Illuminate\Support\Facades\DB::table('roles')->insert([
            'id' => 2,
            'name' => 'Depute with super Admin',
            'super_admin' => 1
        ]);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('roles');
    }
}
