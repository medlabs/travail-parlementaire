<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditTypeRequestForGovRequest extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \Illuminate\Support\Facades\DB::table('gov_requests')
            ->where('request_type', 'Question')
            ->update(['request_type' => 'Written_question']);

        \Illuminate\Support\Facades\DB::table('gov_requests')
            ->where('request_type', 'Answer')
            ->update(['request_type' => 'Answer_to_a_written_question']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \Illuminate\Support\Facades\DB::table('gov_requests')
            ->where('request_type', 'Written_question')
            ->orWhere('request_type', 'Oral_question')
            ->orWhere('request_type', 'Direct_messaging')
            ->update(['request_type' => 'Question']);
        \Illuminate\Support\Facades\DB::table('gov_requests')
            ->where('request_type', 'Answer_to_a_written_question')
            ->update(['request_type' => 'Answer']);
    }
}
